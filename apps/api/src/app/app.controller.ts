import { Controller, Get } from '@nestjs/common';

import { Message } from '@test-semantic-monorepo/api-interface';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('hello')
  getData(): Message {
    return { hello: 'hallo', ...this.appService.getData() };
  }
}
