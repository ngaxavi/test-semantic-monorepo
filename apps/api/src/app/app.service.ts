import { Injectable } from '@nestjs/common';
import { Message } from '@test-semantic-monorepo/api-interface';

@Injectable()
export class AppService {
  getData(): Message {
    return { message: 'Welcome to api!', hello: 'hallo api' };
  }
}
