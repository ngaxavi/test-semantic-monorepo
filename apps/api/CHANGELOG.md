# [api-v1.1.2](https://gitlab.com/ngaxavi/test-semantic-monorepo/compare/api@1.1.1...api@1.1.2) (2019-07-03)


### Bug Fixes

* changelog ([415a635](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/415a635))

# [api-v1.1.1](https://gitlab.com/ngaxavi/test-semantic-monorepo/compare/api@1.1.0...api@1.1.1) (2019-07-03)


### Bug Fixes

* message api ([5f5c62e](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/5f5c62e))

# [api-v1.1.0](https://gitlab.com/ngaxavi/test-semantic-monorepo/compare/api@1.0.1...api@1.1.0) (2019-07-03)


### Features

* message api ([42f3021](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/42f3021))

# [api-v1.0.1](https://gitlab.com/ngaxavi/test-semantic-monorepo/compare/api@1.0.0...api@1.0.1) (2019-07-03)


### Bug Fixes

* api controller ([4419420](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/4419420))

# api-v1.0.0 (2019-07-03)


### Bug Fixes

* api service and test ([a90b561](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/a90b561))
* change name api ([72bde4b](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/72bde4b))
* change name api and release hook ([98701c8](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/98701c8))


### Features

* add change ([c5954f8](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/c5954f8))
* add change ([a4fc2b3](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/a4fc2b3))
* add change ([39760ac](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/39760ac))
* add some changes in apps ([c133932](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/c133932))
* change name and modify text ([6dbd1cb](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/6dbd1cb))
