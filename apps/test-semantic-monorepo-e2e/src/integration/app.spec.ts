import { getGreeting } from '../support/app.po';

describe('test-semantic-monorepo', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to test-semantic-monorepo!');
  });
});
