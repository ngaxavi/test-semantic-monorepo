# [test-semantic-monorepo-v1.0.3](https://gitlab.com/ngaxavi/test-semantic-monorepo/compare/test-semantic-monorepo@1.0.2...test-semantic-monorepo@1.0.3) (2019-07-03)


### Bug Fixes

* changelog ([415a635](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/415a635))

# [test-semantic-monorepo-v1.0.2](https://gitlab.com/ngaxavi/test-semantic-monorepo/compare/test-semantic-monorepo@1.0.1...test-semantic-monorepo@1.0.2) (2019-07-03)


### Bug Fixes

* message web and add bot user to release new version ([379a841](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/379a841))

# [test-semantic-monorepo-v1.0.1](https://gitlab.com/ngaxavi/test-semantic-monorepo/compare/test-semantic-monorepo@1.0.0...test-semantic-monorepo@1.0.1) (2019-07-03)


### Bug Fixes

* new message web ([c189ab1](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/c189ab1))

# test-semantic-monorepo-v1.0.0 (2019-07-03)


### Bug Fixes

* change name in package ([794adf0](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/794adf0))
* change name web ([8e3d0c6](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/8e3d0c6))
* change name web and test release with lerna ([984179c](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/984179c))
* releaserc ([e46e397](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/e46e397))


### Features

* add change ([c5954f8](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/c5954f8))
* add change ([a4fc2b3](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/a4fc2b3))
* add change ([39760ac](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/39760ac))
* add some changes in apps ([c133932](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/c133932))
* change name and modify text ([6dbd1cb](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/6dbd1cb))
* change name web ([0a65e37](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/0a65e37))
* new message ([0b9332e](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/0b9332e))
* new message web ([4e1b6bf](https://gitlab.com/ngaxavi/test-semantic-monorepo/commit/4e1b6bf))
