module.exports = {
  name: 'test-semantic-monorepo',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/test-semantic-monorepo',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
