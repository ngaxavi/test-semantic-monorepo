const hooks = require('semantic-release-monorepo-hooks');
const output = hooks();

const publish = output.isLastModified
	? [
			'@semantic-release/gitlab',
			[
				'@semantic-release/npm',
				{
					npmPublish: false
				}
			]
		]
	: [
			[
				'@semantic-release/npm',
				{
					npmPublish: false
				}
			]
		];

module.exports = {
	branch: 'master',
	tagFormat: output.package + '@${version}',
	prepare: [
		'@semantic-release/changelog',
		[
			'@semantic-release/npm',
			{
				npmPublish: false
			}
		],
		{
			path: '@semantic-release/git',
			message: 'chore(' + output.package + '): release ${nextRelease.version}\n\n${nextRelease.notes}'
		}
	],
	publish: publish,
	verifyConditions: [
		[
			'@semantic-release/npm',
			{
				npmPublish: false
			}
		],
		'@semantic-release/gitlab'
	],
	success: false,
	fail: false,
	monorepo: {
		analyzeCommits: [ '@semantic-release/commit-analyzer' ],
		generateNotes: [ '@semantic-release/release-notes-generator' ]
	}
};
