# 📦🚀 Test Semantic Monorepo



<img src="./docs/nx-logo.png" width="200px"/> <img src="./docs/semantic.png" width="200px"/> <img src="./docs/gitlab.png" width="300px"/>



This is a sample project to handle the problem of releasing many applications with different versions using semantic-release in a monorepo.

## Problem

It's difficult to release applications with different versions applying semantic-release into a project generated employing [nx](https://nx.dev) because semantic-release does not support mono repository.

## Installation

```bash
npm i -D semantic-release semantic-release-monorepo semantic-release-monorepo-hooks
npm i -D @semantic-release/{changelog,git}
```

If you use another git repository hosting such as GitLab,  you have to install the corresponding plugin

```bash
npm i -D @semantic-release/gitlab
```



## Configuration

1. **Configure** `**.releaserc.js**`

   If your repository is hosted by GitHub you don't need  the keys **success** and **fail** in **module.exports**. You can remove them. If this is not the case, set their value to **false**.

   > **npmPublish: false** [ update version in the corresponding package.json but don't publish your package to NPM registry. ]

```js
const hooks = require('semantic-release-monorepo-hooks');
const output = hooks();

const publish = output.isLastModified
	? [ '@semantic-release/gitlab',  // for GitHub use @semantic-release/github
		['@semantic-release/npm', { npmPublish: false }] ]
		:[
			['@semantic-release/npm', { npmPublish: false }] ];

module.exports = {
	branch: 'master',
	tagFormat: output.package + '@${version}',
	prepare: [
		'@semantic-release/changelog',
		['@semantic-release/npm', { npmPublish: false }],
		{
			path: '@semantic-release/git',
			message: 'chore(' + output.package + '): release ${nextRelease.version}\n\n${nextRelease.notes}'
		}
	],
	publish: publish,
	verifyConditions: [
		['@semantic-release/npm', { npmPublish: false }],
		'@semantic-release/gitlab' // for GitHub use @semantic-release/github
	],
	success: false,
	fail: false,
	monorepo: {
		analyzeCommits: [ '@semantic-release/commit-analyzer' ],
		generateNotes: [ '@semantic-release/release-notes-generator' ]
	}
};

```

Setting the value of the variable **tagFormat** is important in order to be able to release a new version of a specific application. You always have to prepend the name of the application in question (see above).

2. **Configure** `**.gitlab-ci.yml**`

   ```yaml
   .init_script: &init_script |
     # Initialize functions
     [[ "$TRACE" ]] && set -x
   
     function release() {
       echo "👷 Releasing affected apps..."
       CURRENT_PATH=$(pwd)
       echo "current path: $CURRENT_PATH"
       POP=`npm run affected:apps -s -- $CI_COMMIT_BEFORE $CI_COMMIT_SHORT_SHA`
       echo $POP
       for app in `npm run affected:apps -s -- $CI_COMMIT_BEFORE $CI_COMMIT_SHORT_SHA`; do
         echo "🏗 Release ${app} in progress..."
         if [[ "${app}" == $API ]]; then
           cd $CURRENT_PATH/apps/$API
           npx semantic-release -e semantic-release-monorepo
           echo "🎉 API: new version has been released 💯"
         fi
         if [[ "${app}" = $MONO ]]; then
           cd $CURRENT_PATH/apps/$MONO
           npx semantic-release -e semantic-release-monorepo
           echo "🎉 Test: new version semantic has been released 💯"
         fi
       done
     }
   
   
   image: node:12
   
   variables:
     API: "api"
     MONO: "test-semantic-monorepo"
   
   stages:
     - release
   
   before_script:
     - *init_script
   
   release:
     stage: release
     script:
       - export CI_COMMIT_BEFORE=$(git rev-parse --short $CI_COMMIT_SHORT_SHA^)
       - echo $CI_COMMIT_BEFORE
       - echo $CI_COMMIT_SHORT_SHA
       - npm ci
       - release
     only:
       - master
     except:
       variables:
         - "$CI_COMMIT_MESSAGE =~ /chore(.*): release.*$/"
   ```

   The variable **CI_COMMIT_SHORT_SHA** represents the short commit sha of the last commit.

   You also need to find a previous commit that was present on the master branch before a push request (**CI_COMMIT_BEFORE**). I know that this has already been predefined into gitlab-ci but it doesn't work for me (it always shows 000000. Check **CI_COMMIT_BEFORE_SHA**). With the previous commit sha you can find to which applications the changes have been applied. This can be done by the **release** shell function.

   Moreover, the **release** function requires the semantic-release in order to generate the next release notes of an application using the commit messages. However, the commit messages must follow [Angular Commit Message Conventions](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines) . Tools such as [commitizen](https://github.com/commitizen/cz-cli), [commitlint](https://github.com/conventional-changelog/commitlint) or [semantic-gi-commit-cli ](https://github.com/JPeer264/node-semantic-git-commit-cli) can be employed to help developers and enforce valid commit messages.

